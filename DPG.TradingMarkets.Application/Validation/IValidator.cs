﻿using DPG.TradingMarkets.Application.Contract.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.TradingMarkets.Application.Validation.Core
{
    public interface IValidator<T>
    {
        IValidationResult Validate(T item);
    }
}
