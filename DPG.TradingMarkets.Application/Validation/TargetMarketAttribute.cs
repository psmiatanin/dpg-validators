﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.TradingMarkets.Application.Validation.Core
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
    public class TargetMarketAttribute : Attribute
    {
        public string Market { get; }

        public TargetMarketAttribute(string market)
        {
            if (string.IsNullOrWhiteSpace(market))
            {
                throw new ArgumentException("Value must not be null or white spaced string", nameof(market));
            }

            Market = market;
        }
    }
}
