﻿using DPG.TradingMarkets.Application.Contract.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.TradingMarkets.Application.Validation.Core
{
    public class ValidationResult : IValidationResult
    {
        private readonly List<ValidationError> _errors = new List<ValidationError>();

        public bool Success
        {
            get
            {
                return _success.HasValue ? _success.Value : _errors.Count != 0;
            }
            set
            {
                _success = value;
            }
        }
        private bool? _success = null;

        public IEnumerable<ValidationError> Errors => _errors;

        public void AddError(ValidationError ve)
        {
            if (ve == null)
            {
                throw new ArgumentNullException();
            }

            _errors.Add(ve);
        }
    }
}
