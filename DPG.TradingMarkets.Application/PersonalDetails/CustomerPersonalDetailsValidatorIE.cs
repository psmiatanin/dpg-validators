﻿using DPG.TradingMarkets.Application.Contract.Model;
using DPG.TradingMarkets.Application.Contract.Validation;
using DPG.TradingMarkets.Application.Validation.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.TradingMarkets.Application.PersonalDetails
{
    [TargetMarket(Markets.IE)]
    public class CustomerPersonalDetailsValidatorIE : IValidator<CustomerPersonalDetails>
    {
        public IValidationResult Validate(CustomerPersonalDetails item)
        {
            return new ValidationResult();
        }
    }
}
