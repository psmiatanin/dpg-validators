﻿using DPG.TradingMarkets.Application.Contract.Model;
using DPG.TradingMarkets.Application.Contract.Validation;
using DPG.TradingMarkets.Application.Validation.Core;

namespace DPG.TradingMarkets.Application.PersonalDetails
{
    [TargetMarket(Markets.UK)]
    public class CustomerPersonalDetailsValidatorUK : IValidator<CustomerPersonalDetails>
    {
        public IValidationResult Validate(CustomerPersonalDetails item)
        {
            return new ValidationResult();
        }
    }
}
