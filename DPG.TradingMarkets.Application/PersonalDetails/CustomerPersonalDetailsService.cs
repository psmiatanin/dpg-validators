﻿using DPG.TradingMarkets.Application.Contract.Model;
using DPG.TradingMarkets.Application.Contract.PersonalDetails;
using DPG.TradingMarkets.Application.Validation.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.TradingMarkets.Application.PersonalDetails
{
    public class CustomerPersonalDetailsService : ICustomerPersonalDetailsService
    {
        private readonly IValidator<CustomerPersonalDetails> _validator;

        public CustomerPersonalDetailsService(IValidator<CustomerPersonalDetails> validator)
        {
            _validator = validator;
        }
    }
}
