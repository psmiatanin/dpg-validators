﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DPG.TradingMarkets.Application.Validation.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Reflection;
using DPG.TradingMarkets.Application.Contract.PersonalDetails;
using DPG.TradingMarkets.Application.PersonalDetails;
using DPG.TradingMarkets.WebApi;

namespace DPG.TradingMarkets
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            ConfigureValidators(services);

            services.AddScoped<ICustomerPersonalDetailsService, CustomerPersonalDetailsService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        private void ConfigureValidators(IServiceCollection services)
        {
            var types = typeof(TargetMarketAttribute).Assembly.GetTypes()
                .Where(it => !it.IsAbstract && it.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IValidator<>)))
                .ToArray();

            var registrations = new Dictionary<Type, Dictionary<string, Type>>();

            foreach (var it in types)
            {
                services.AddTransient(it);

                var interfaces = it.GetInterfaces()
                    .Where(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IValidator<>));

                foreach (var ifc in interfaces)
                {
                    if (!registrations.ContainsKey(ifc))
                    {
                        registrations.Add(ifc, new Dictionary<string, Type>());
                    }

                    foreach (var attribute in it.GetCustomAttributes<TargetMarketAttribute>())
                    {
                        registrations[ifc].Add(attribute.Market, it);
                    }
                }
            }

            var market = "IE";

            foreach (var it in registrations.Keys)
            {
                services.AddTransient(it, provider =>
                {
                    return provider.GetService(registrations[it][market]);
                });
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseMiddleware<ExceptionMiddleware>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
