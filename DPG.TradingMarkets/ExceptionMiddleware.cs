﻿using DPG.TradingMarkets.Application.Contract.Validation;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DPG.TradingMarkets.WebApi
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (ValidationException ex)
            {
                httpContext.Response.ContentType = "application/json";
                httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;

                var content = JsonConvert.SerializeObject(ex.ValidationResult);
                await httpContext.Response.WriteAsync(content);
            }
            catch (Exception ex)
            {
                // TODO: ...
                throw;
            }
        }
    }
}
