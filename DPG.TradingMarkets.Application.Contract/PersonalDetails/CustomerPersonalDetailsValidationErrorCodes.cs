﻿using DPG.TradingMarkets.Application.Contract.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.TradingMarkets.Application.Contract.Model
{
    public class CustomerPersonalDetailsValidationErrorCodes
        : ValidationErrorCodes
    {
        // TODO: Add here error codes which are specific
        // for customer personal details
        // ...
    }
}