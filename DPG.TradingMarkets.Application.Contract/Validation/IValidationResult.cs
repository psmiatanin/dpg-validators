﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.TradingMarkets.Application.Contract.Validation
{
    public interface IValidationResult
    {
        bool Success { get; }

        IEnumerable<ValidationError> Errors { get; }
    }
}
