﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DPG.TradingMarkets.Application.Contract.Validation
{
    /// <summary>
    /// The class has generic error codes. Use it as base class
    /// for any class with error codes
    /// </summary>
    public class ValidationErrorCodes
    {
        [Description("Value is required")]
        public const int ValueIsRequired = 100;

        [Description("Value has invalid format")]
        public const int InvalidValueFormat = 101;
    }
}
