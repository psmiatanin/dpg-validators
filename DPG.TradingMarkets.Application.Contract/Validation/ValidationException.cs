﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.TradingMarkets.Application.Contract.Validation
{
    /// <summary>
    /// TODO: The exception has invalid implementation.
    /// It is just only basis for further implementation
    /// </summary>
    public class ValidationException : Exception
    {
        public IValidationResult ValidationResult { get; private set; }

        public ValidationException()
            : base()
        { }

        public ValidationException(IValidationResult validationResult)
            : base()
        {
            ValidationResult = validationResult;
        }

        public ValidationException(string message) : base(message)
        {
        }

        public ValidationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
