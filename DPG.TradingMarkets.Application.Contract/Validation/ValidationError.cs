﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DPG.TradingMarkets.Application.Contract.Validation
{
    public class ValidationError
    {
        public int ErrorCode { get; }

        public string ErrorMessage { get; set; }

        public string Property { get; set; }

        public ValidationError(int errorCode, string errorMessage = null, string property = null)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
            Property = property;
        }
    }
}
